This folder is for the NIST ReAdw4Mascot2 executable (ver. 20091016a) and needed libraries.


The folder must contain:

- ReAdw4Mascot2.exe (available in NIST MSQC Pipeline)
- zlib1.dll

ReAdW4Mascot2 Author:
The program belongs to Patrick Pedrioli
ReAdW4Mascot2 is part of the NIST MSQC Pipeline, which can be downloaded from:
http://peptide.nist.gov/software/nist_msqc_pipeline/NIST_MSQC_Pipeline.html

zlib1.dll (vers 1.2.3) can be obtained from:
http://www.zlib.net/