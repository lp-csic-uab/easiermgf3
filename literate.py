import wx

try:
    from agw import balloontip as agw_bt
except ImportError:  # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.balloontip as agw_bt


class Literate:
    # noinspection PyArgumentEqualDefault,PyUnresolvedReferences
    def __init__(self, hints):
        
        # noinspection PyUnusedLocal
        title_font = wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, True)
        hint_font = wx.Font(8, wx.SWISS, wx.ITALIC, wx.NORMAL, False)
        
        for option, values in hints.items():
            (name, hint) = values[0:2]
            
            try:
                widget = getattr(self, name)
            except AttributeError:
                continue
            
            try:
                default = values[2]
            except IndexError:
                pass
            else:
                try:
                    widget.SetValue(default)
                except TypeError:
                    widget.SetValue(bool(default))

            title = "Option: -%s\nWidget: %s" % (option, name)

            balloon = agw_bt.BalloonTip(topicon=None, toptitle=title,
                                        message=hint, shape=agw_bt.BT_ROUNDED,
                                        tipstyle=agw_bt.BT_LEAVE)
            
            # Set The Target
            balloon.SetTarget(widget)
            # Set The Balloon Colour
            balloon.SetBalloonColour(None)
            # Set The Font For The Top Title
            balloon.SetTitleFont(None)
            # Set The Colour For The Top Title
            balloon.SetTitleColour(wx.RED)
            # Set The Font For The Tip Message
            balloon.SetMessageFont(hint_font)
            # Set The Colour For The Tip Message
            balloon.SetMessageColour(None)
            # Set The Delay After Which The BalloonTip Is Created
            balloon.SetStartDelay(1000)
