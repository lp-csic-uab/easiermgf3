import os
import base64
from array import array
from collections import namedtuple
from collections import defaultdict
import xml.etree.ElementTree as et
from xml.parsers.expat import ExpatError
from easier_raw_class import RawFileExtractor, MockExtractor
#
#
#
def decode(text):
    """Decode spectrum string extracted from a mzXML file."""
    spec = array('f')
    spec.fromstring(base64.b64decode(text))
    spec.byteswap()
    return spec
#
#
def format_spectrum(spectrum):
    """Convert a spectrum in the format [mz1, int1...] into ["mz1 int1"...].

    entry    [mz1, int1, mz2, int2, ...]
    returns ['mz1 int1', 'mz2 int2', 'mz3 int3',...]

    """
    new_spectrum = []
    i = 0
    while True:
        try:
            intensity = spectrum[i + 1]
        except IndexError:
            break

        new_spectrum.append('%s %s' % (spectrum[i], intensity))
        i += 2

    return new_spectrum
#
#
# noinspection PyUnboundLocalVariable
def iter_mzxml_data_fqs(xml):
    """iterates over scan elements in ReAdW mzXML file (scan cycle type full->quant->seq).

    yield spectrum and params ('scan, mslevel, rt, charge, method, mz, spec')

    the process assumes a scan series of the type:

        full_scan + (qscan * n + (cid_scan + (cid_ms3 * y))) * m
            m  number of selected precursors (m>0)
            n  average of quantification spectra used (n>=0)
            y  number of ms3 (y >= 0)

    - it returns ms2 scans with HCD/PQD quantitative data already merged
    - merged ms2 scans indicate scan_range start
    - ms3 DON'T also add quant, this is made by integrator ?
    wouldn't be better to do it here? p.e. assign a quantification to each
    spectrum number taking it separately and linking it at the end, after
    identification?, this would not affect search.
    - ms3 scans don't indicate range. Parent mass reference is lost ! later has
    to be found again by integrator...

    """
    #
    try:
        doc = et.parse(xml)
    except ExpatError as e:
        print("File %s produces error\n'%s'" % (xml, e))
        return
    #
    root = doc.getroot()
    children = root.getchildren()
    msRun = children[0]
    #
    for msRun_child in msRun.getchildren():
        if msRun_child.tag.endswith('scan'):
            msms_data_list = process(msRun_child)
        else:
            continue

        qspecs = []
        # print msRun_child.tag
        for scan in msms_data_list:
            # print scan
            spec_data = decode(scan.spec)
            spec = format_spectrum(spec_data)
            scan_number = scan.scan

            if (scan.method == 'HCD') or (scan.method == 'PQD'):
                # first quantification scan (qscan)
                if not qspecs:
                    start_qscan = scan_number
                    qspecs = [spec]
                # the following qscans
                else:
                    qspecs.append(spec)

            elif scan.method == 'CID':
                rt = float(scan.rt[2:-1])
                rt = '%.4f' % (rt / 60)
                #
                # if getting this cid spectrum after several qscans
                if qspecs:
                    qspec = average(qspecs)    # this just adds ions masses,
                                               # does not sum intensities for ions of slighly different mass which
                                               # putatively are the same qtag ion

                    scan_range = "%s-%s" % (start_qscan, scan_number)
                    spec = add_quant_to_cid(qspec, spec, 0)
                    qspecs = None
                #
                # no qscans or ms3
                else:
                    scan_range = scan_number

                params = (0, scan.charge, rt,
                          scan.mslevel, scan_range, scan.mz)

                yield spec, params
#
#
def iter_mzxml_data_fsq(xml):
    """iterates over scan elements in ReAdW mzXML file (scan cycle type full->seq->quant).

    yield spectrum and params ('scan, mslevel, rt, charge, method, mz, spec')

    the process assumes a scan series of the type:

        full_scan + (cid_scan + hcd_scan) * m
            m  number of selected precursors (m>0)
            n  average of quantification spectra used (n>=0)
            y  number of ms3 (y >= 0)

    - it returns ms2 scans with HCD/PQD quantitative data already merged
    - merged ms2 scans indicate scan_range start

    """
    #
    try:
        doc = et.parse(xml)
    except ExpatError as e:
        print("File %s produces error\n'%s'" % (xml, e))
        return
    #
    root = doc.getroot()
    children = root.getchildren()
    msRun = children[0]
    #
    for msRun_child in msRun.getchildren():
        if msRun_child.tag.endswith('scan'):
            msms_data_list = process(msRun_child)
        else:
            continue

        #print msRun_child.tag
        for scan in msms_data_list:
            #print scan
            spec_data = decode(scan.spec)
            spec = format_spectrum(spec_data)
            scan_number = scan.scan

            if scan.method == 'CID':
                rt = float(scan.rt[2:-1])
                rt = '%.4f' % (rt / 60)

                start_scan = scan_number
                s_spec = spec

            elif (scan.method == 'HCD') or (scan.method == 'PQD'):
                q_spec = spec

                scan_range = "%s-%s" % (start_scan, scan_number)
                spec = add_quant_to_cid(q_spec, s_spec, 0)

                params = (0, scan.charge, rt,
                          scan.mslevel, scan_range, scan.mz)

                yield spec, params
#
#
def average(spec_list):
    """Extract quantification data from a spectrum list.

    Do not mind being TMT or ITRAQ, just add ions.
    Quantification is made in Integrator although it could be not a good idea
    because Integrator do it blindly without taking into account mass precision
    and other problems (already resolved in psearch!, a shame!!)

    Spectra are of the form:
    [['mz1 int1','mz2 int2','mz3 int3',...], ....]

    """

    if len(spec_list) > 1:
        spec_dict = defaultdict(float)

        for spec in spec_list:
            for value in spec:
                mass, int = value.split()
                spec_dict[float(mass)] += float(int)

        masses = sorted(spec_dict.keys())
        spec = ['%f %f' % (mass, spec_dict[mass]) for mass in masses]

    else:
        spec = spec_list[0]

    return spec
#
#
#noinspection PyUnboundLocalVariable,PyUnusedLocal
def build_mgfs_from_mzxml(xml, mgf, temp_dir, result_dir,
                          remove_z_one, split_2_3, add_quant, mode='fqs'):
    """Build custom mgf from ReAdW mzXML

    - <temp_dir> is a dir where mzxml is read and mgfs are saved
    - <result_dir> is where mgfs go and maybe mzxml is saved
    - <add_quant> is not used
    - <mode> (fqs/fsq) considers if CID scans are before (fsq) or after (fqs) hcd/pqd scans

    """
    xml_path = os.path.join(temp_dir, xml)
    (xml_basename, _) = xml.rsplit('.')

    if mode == 'fqs':
        # print 'running normal mode 1 pqd/hcd -> 1 cid'
        iter_mzxml_data = iter_mzxml_data_fqs
    else:
        # print 'running maciek mode 1 cid-> 1 pqd/hcd'
        iter_mzxml_data = iter_mzxml_data_fsq

    file_handles = get_file_handles(result_dir, mgf, split_2_3, remove_z_one)

    if split_2_3 and remove_z_one:
        (fh12, fh13, fh2, fh3) = file_handles
    elif split_2_3:
        (fh2, fh3) = file_handles
    elif remove_z_one:
        (fh123, fh231) = file_handles
    else:
        (fh,) = file_handles

    for spectrum, (mass, charge, rt, ms, sc, mz) in iter_mzxml_data(xml_path):
        head_hcd = ['']
        head_hcd.extend(spectrum)
        head_hcd.append('END IONS\n\n')
        spectrum = build_mgf(head_hcd, mass, charge,
                             xml_basename, rt, ms, sc, mz)
        spectrum[0] = spectrum[0].rstrip()
        spectrum = '\n'.join(spectrum)

        if split_2_3 and remove_z_one:
            if charge == '1':
                if ms == '2':
                    fh12.write(spectrum)
                elif ms == '3':
                    fh13.write(spectrum)
            else:
                if ms == '2':
                    fh2.write(spectrum)
                elif ms == '3':
                    fh3.write(spectrum)
        elif split_2_3:
            if ms == '2':
                fh2.write(spectrum)
            elif ms == '3':
                fh3.write(spectrum)
        elif remove_z_one:
            if charge == '1':
                fh123.write(spectrum)
            else:
                fh231.write(spectrum)
        else:
            fh.write(spectrum)

    for handle in file_handles:
        handle.close()
#
#
def process(scan_element):
    """Process a msRun scan child.

    returns a list of type:
    [pqd, pqd, cid, ms3, ..., pqd, pqd, cid, ms3]
    """

    if scan_element.attrib.get('msLevel') == '1':
        children = scan_element.getchildren()
        msms = [item for item in children if item.tag.endswith('scan')]
    else:
        print('return. is msLevel ', scan_element.attrib.get('msLevel'))
        return

    msms_data = []
    for ms2 in msms:
        data = get_scandata(ms2)
        msms_data.append(data)
        ms3 = [ms for ms in ms2.getchildren() if ms.tag.endswith('scan')]
        for ms in ms3:
            data = get_scandata(ms)
            msms_data.append(data)

    return msms_data
#
#
def get_scandata(ms):
    """return named tuple of scan data"""
    ScanData = namedtuple('ScanData',
                          'scan, mslevel, rt, charge, method, mz, spec')

    ms_children = ms.getchildren()
    return ScanData(ms.attrib.get('num'),
                    ms.attrib.get('msLevel'),
                    ms.attrib.get('retentionTime'),
                    ms_children[0].attrib.get('precursorCharge'),
                    ms_children[0].attrib.get('activationMethod'),
                    ms_children[0].text,
                    ms_children[1].text)
#
#
# noinspection PyUnboundLocalVariable
def build_mgf_from_dtas(dta_dir,
                        raw, outfile, remove_z_one, split_2_3, add_quant):
    """From a dta just containing:

    1) filename
    (filename.scan_init.scan_end.charge)
    'test_DIGE.120.125.2.dta'
    'test_DIGE.105.105.2.dta'     (non-grouped)

    2) header dta
    (peptide_Mr charge)
    996.70949 2

    yields mgf with header like:

    BEGIN IONS
    TITLE=test_DIGE,Scan:120-125,MS:2,Rt:1833.494
    PEPMASS=419.32                          (peptide m/z ?)
    SCANS=120-125
    RTINSECONDS=1833.494

    Used by ExtractMsn classes in easier_mgf

    """

    dtas = os.listdir(dta_dir)
    dtas = [arch for arch in dtas if arch.endswith('.dta')]
    dtas.sort(key=key_sort)

    raw_data = RawFileExtractor(raw)
    if not raw_data.success:
        raw_data = MockExtractor()

    tempdir, outfile = os.path.split(outfile)

    if split_2_3:
        out_handle_2 = fhopen(tempdir, 'ms2_', outfile)
        out_handle_3 = fhopen(tempdir, 'ms3_', outfile)
    else:
        out_handle = fhopen(tempdir, '', outfile)

    hay_hcd = False
    END = 'END IONS\n'
    for dta in dtas:

        try:
            (raw_name, scan_ini, scan_end, charge, ext) = dta.split('.')
        except ValueError:
            print('error')
            continue

        if remove_z_one and (charge == 1):
            continue

        if scan_ini == scan_end:
            group_scans = scan_ini
        else:
            group_scans = '%s-%s' % (scan_ini, scan_end)

        # continue if scan has been already used  ??
        # FTMS + c NSI d Full ms2 1070.52@hcd59.00 [110.00-2000.00]

        dta_path = os.path.join(dta_dir, dta)
        handle = open(dta_path)
        lines = handle.readlines()
        header = lines[0]
        (mass, charge) = header.strip().split()
        (mode, ms) = raw_data.get_mode_and_ms_level(scan_end)
        rt = raw_data.get_rt(scan_end)
        ms = ms[-1]

        if (mode == 'hcd') and add_quant:
            # print('hcd en ', group_scans)
            hcd_mass_charge = (mass, charge)
            hcd_dta = lines[:]
            hay_hcd = True
            continue

        if hay_hcd and ((mass, charge) == hcd_mass_charge):
            lines = add_quant_to_cid(hcd_dta, lines, 1)
            lines.append(END)
            lines = build_mgf(lines, mass, charge,
                              raw_name, rt, ms, group_scans)
            hay_hcd = False
        elif hay_hcd:
            hcd_dta.append(END)
            lines = build_mgf(hcd_dta, mass, charge,
                              raw_name, rt, ms, group_scans)

            if split_2_3:
                if ms == '2':
                    out_handle_2.write(''.join(lines) + '\n')
                elif ms == '3':
                    out_handle_3.write(''.join(lines) + '\n')
            else:
                out_handle.write(''.join(lines) + '\n')

            lines.append(END)
            lines = build_mgf(lines, mass, charge,
                              raw_name, rt, ms, group_scans)
            hay_hcd = False
        else:
            lines.append(END)
            lines = build_mgf(lines, mass, charge,
                              raw_name, rt, ms, group_scans)

        if split_2_3:
            if ms == '2':
                out_handle_2.write(''.join(lines) + '\n')
            elif ms == '3':
                out_handle_3.write(''.join(lines) + '\n')
        else:
            out_handle.write(''.join(lines) + '\n')

    raw_data.close()

    if split_2_3:
        out_handle_2.close()
        out_handle_3.close()
    else:
        out_handle.close()
#
#
def add_quant_to_cid(hcd, cid, start_line):
    """Mix HCD/PQD quantitative ions with CID scan."""
    last_line = first_line = start_line
    for index, hcd_line in enumerate(hcd[start_line:]):
        mass, other = hcd_line.split()
        if float(mass) > 150:
            last_line = index + start_line
            break

    for index, cid_line in enumerate(cid[start_line:]):
        mass, other = cid_line.split()
        if float(mass) > 150:
            first_line = index + start_line
            break

    try:
        new_dta = cid[:start_line] +\
                  hcd[start_line:last_line] + cid[first_line:]
    # sometimes the spectrum have no signals and
    # lastline and/or first_line are not assigned
    except UnboundLocalError:
        new_dta = ['0 0']

    return new_dta
#
#
def build_mgf(lines, mass, charge, raw_name, rt, ms, group_scans, mz=None):
    """Build and add a custom mgf header to an spectrum

    the spectrum is in the form of a list of lines with first line available
    for the new header (no info must be lost)
    """
    FORMAT = ('BEGIN IONS\n'
              'TITLE=%s,Scan:%s,MS:%s,Rt:%s\n'
              'PEPMASS=%s\n'
              'SCANS=%s\n'
              'RTINSECONDS=%s\n')

    if not mz:
        mz = (float(mass) + (int(charge) - 1) * 1.007825) / int(charge)

    text = FORMAT % (raw_name, group_scans, ms, rt,
                     mz,
                     group_scans,
                     rt)

    lines[0] = text

    return lines
#
#
def key_sort(name):
    """Key for sorting dtas on the basis on the scan number in the name"""
    splitted = name.split('.')
    return int(splitted[1])
#
#
def fhopen(root, prefix, fname):
    """for safe opening of new files in append mode"""
    fpath = os.path.join(root, prefix + fname)
    if os.path.exists(fpath):
        os.remove(fpath)

    return open(fpath, 'a')
#
#
def get_file_handles(temp, fname, split_2_3, remove_z_one):
    """Return the file handles to the different output files"""
    if split_2_3 and remove_z_one:
        fh12 = fhopen(temp, '1ms2_', fname)
        fh13 = fhopen(temp, '1ms3_', fname)
        fh2 = fhopen(temp, '2ms21_', fname)
        fh3 = fhopen(temp, '2ms31_', fname)
        return fh12, fh13, fh2, fh3

    elif split_2_3:
        fh2 = fhopen(temp, 'ms2_', fname)
        fh3 = fhopen(temp, 'ms3_', fname)
        return fh2, fh3

    elif remove_z_one:
        fh123 = fhopen(temp, '1ms23_', fname)
        fh231 = fhopen(temp, 'ms231_', fname)
        return fh123, fh231

    else:
        fh = fhopen(temp, 'out_', fname)
        return fh
#
#
#
if __name__ == "__main__":

    dta_dir = 'C:/Python26/programas/_easier/extract_msn_temp/test_DIGE'
    raw = 'C:/Python26/programas/_easier/test_mgf/test_DIGE.RAW'
    outfile = 'C:/Python26/programas/_easier/test_mgf/mgf_from_dta'

    build_mgf_from_dtas(dta_dir, raw, outfile, True, True, True)
