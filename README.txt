EasierMgf vs 0.1

  EasierMgf has been coded in Python
      by Joaquin Abian at LP-CSIC/UAB ( lp.csic@uab.cat )

      December 2013


EasierMgf Converts RAW Thermo Files into MGF files
from LTQ XL, LTQ Velos and Orbitraps.

For this it uses four different converters:

***Extract_msn (v 4.0)****
Compatible with Thermo Xcalibur 2.0.7
(LTQ XL)

To make this converter work, the folder exe_extract_msn\ must contain the extract_msn  executable (ver. 4.0) and needed libraries:

- extract_msn.exe  (extract_msn ver. 4.0) (http://sjsupport.thermofinnigan.com/public/index_download_customer.asp)
- Fileio.dll
- Fregistry.dll
- UnifiedFile.dll  (ver. 2007) (http://www.matrixscience.com/help/instruments_xcalibur.html#MSF)

All those files can be obtained from a Thermo Xcalibur 2.0.7 installation (Folder C:\Xcalibur\system\programs), 
altought some of them (Fileio.dll , Fregistry.dll) can be obtained from MSFileReader package, which can be downloaded from:
- http://sjsupport.thermofinnigan.com/download/files/MSFileReaderSetup.zip

@TO-DO

****Extract_msn_com (v 5.0)****
To make this converter work, the folder exe_extract_msn_com\ must contain the extract_msn_com executable and needed libraries:

- extract_msn_com.exe (extract_msn ver. 5.0)
- UnifiedFile.dll (ver. 2008)

Both files can be downloaded in a zip from Thermo webpage at:
- http://sjsupport.thermofinnigan.com/public/detail.asp?id=626
And the direct download url is:
- http://sjsupport.thermofinnigan.com/download/files/ExtractMSnCom.zip

To run the program it is also necessary to install following Thermo libraries provided by Xcalibur or MSFileReader:

- Fileio.dll
- Fregistry.dll
- XRawfile2.dll

MSFileReader is a standalone installation of XRawfile2.dll (for Xcalibur), which permits
programmatic access of Thermo data files via a COM interface.

This program can be downloaded from:
- http://sjsupport.thermofinnigan.com/public/detail.asp?id=703
And the direct download url is:
- http://sjsupport.thermofinnigan.com/download/files/MSFileReaderSetup.zip

@TO-DO


****ReAdW******
To make this converter work, the folder exe_readw\ must contain the ReAdW executable (ver. 4.3.1, 2009; available from the Trans-Proteome Pipeline) and needed libraries:

- ReAdW.exe
- zlib1.dll

ReAdW Authors:
Natalie Tasman (SPC/ISB), with Jimmy Eng, Brian Pratt, and Matt Chambers, based on orignal work by Patrick Pedriolli.

More information:
http://tools.proteomecenter.org/wiki/index.php?title=Software:ReAdW

Download from:
http://sourceforge.net/projects/sashimi/files/ReAdW%20%28Xcalibur%20converter%29/ReAdW-4.3.1/ReAdW-4.3.1.zip/download

zlib1.dll (vers 1.2.3) can be obtained from:
http://www.zlib.net/

@TO-DO


****ReAdW4Mascot2******
To make this converter work, the folder exe_readw4mascot2\ must contain the NIST ReAdw4Mascot2 executable (ver. 20091016a; available from the NIST MSQC Pipeline) and needed libraries:

- ReAdw4Mascot2.exe (ver. 20091016a)
- zlib1.dll

ReAdW4Mascot2 Author:
The program belongs to Patrick Pedrioli.

ReAdW4Mascot2 is part of the NIST MSQC Pipeline, which can be downloaded from:
http://peptide.nist.gov/software/nist_msqc_pipeline/NIST_MSQC_Pipeline.html

zlib1.dll (vers 1.2.3) can be obtained from:
http://www.zlib.net/

@TO-DO


*** EasierMgf Licence ***

      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 3 of the License, or
      (at your option) any later version.
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software

      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
      MA 02110-1301, USA.

See LICENCE.txt for more details about licensing of the software (GPL v3).


