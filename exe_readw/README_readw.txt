This folder is for the ReAdW executable (ver. 4.3.1, 2009; from the Trans-Proteome Pipeline) and needed libraries.


The folder must contain:

- ReAdW.exe
- zlib1.dll

ReAdW Authors:
Natalie Tasman (SPC/ISB), with Jimmy Eng, Brian Pratt, and Matt Chambers, based on orignal work by Patrick Pedriolli.
More information:
http://tools.proteomecenter.org/wiki/index.php?title=Software:ReAdW
Download from:
http://sourceforge.net/projects/sashimi/files/ReAdW%20%28Xcalibur%20converter%29/ReAdW-4.3.1/ReAdW-4.3.1.zip/download

zlib1.dll (vers 1.2.3) can be obtained from:
http://www.zlib.net/