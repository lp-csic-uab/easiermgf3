__author__ = 'joaquin'

import unittest
import os
from easier_mgf_builders import build_mgfs_from_mzxml

class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.data_dir = "test/test_data"
        self.result_dir = "test/test_results"
        self.mzxml = "test_TMT_Orbi_ReadW.mzXML"
        self.result = ['1ms2_test_TMT_Orbi_ReadW.mgf',
                       '1ms3_test_TMT_Orbi_ReadW.mgf',
                       '2ms21_test_TMT_Orbi_ReadW.mgf',
                       '2ms31_test_TMT_Orbi_ReadW.mgf'
                      ]

    def test_build_mgfs_from_mzxml(self):
        mgf = "test_TMT_Orbi_ReadW"
        remove_z_one = True
        split_2_3 = True
        add_quant = True
        build_mgfs_from_mzxml(self.mzxml, mgf, self.data_dir, self.result_dir,
                                          remove_z_one, split_2_3, add_quant)

        for arch in self.result:
            target = os.path.join(self.data_dir, arch)
            arch = arch[:-4]
            test = os.path.join(self.result_dir, arch)
            with open(target) as a_target:
                with open(test) as a_test:
                    self.assertEqual(a_target.read(), a_test.read())


if __name__ == '__main__':
    unittest.main()
