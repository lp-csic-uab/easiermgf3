__author__ = 'joaquin'

import wx
import os
import configparser
from easier_folders_panel import FoldersPanel

CONFIG = 'easier.ini'
TEMP = 'temporal'
MGFS = 'mgfs'

# noinspection PyUnusedLocal
class MyFolders(FoldersPanel):
    """A class to save and read configuration params from an config.ini file.

    Params must be recovered from the ConfigParser object self.config instead of
    from direct attributes like self.temp_dir, etc. In this way you will be sure
    read params are those that have been actively saved.

    """

    def __init__(self, *args, **kwargs):
        FoldersPanel.__init__(self, *args, **kwargs)

        self.temp_dir = TEMP
        self.result_dir = MGFS
        self.config_file = CONFIG

        self.Bind(wx.EVT_BUTTON, self.on_save, self.bt_save)
        self.Bind(wx.EVT_BUTTON, self.on_reload, self.bt_reload)
        self.Bind(wx.EVT_BUTTON, self.on_defaults, self.bt_defaults)

        self.Bind(wx.EVT_BUTTON, self.on_select_temp, self.bt_temp)
        self.Bind(wx.EVT_BUTTON, self.on_select_mgf, self.bt_mgf)
        self.Bind(wx.EVT_BUTTON, self.on_select_file, self.bt_ini)
        #
#        self.Bind(wx.EVT_BUTTON, self.on_check, self.lb_title)

        self.config = configparser.ConfigParser()
        if os.path.exists(self.config_file):
            self.config.read(self.config_file)
            self.init_folders()
        else:
            self.build_default_ini()

    def init_folders(self):
        self.temp_dir = self.config.get('directories', 'temporal')
        self.result_dir = self.config.get('directories', 'mgfs')
        self.fill_entries()

    def build_default_ini(self):
        self.set_default_folders()

        self.config.add_section('directories')
        self.config.set('directories', 'temporal', TEMP)
        self.config.set('directories', 'mgfs', MGFS)

        self.write_config_file()
        self.fill_entries()

    def set_default_folders(self):
        self.temp_dir = TEMP
        self.result_dir = MGFS
        self.config_file = CONFIG

    def write_config_file(self):
        with open(self.config_file, 'w') as f:
            self.config.write(f)

    def fill_entries(self):
        self.tc_temp.SetValue(self.temp_dir)
        self.tc_mgf.SetValue(self.result_dir)
        self.tc_ini.SetValue(self.config_file)
    #
    def on_reload(self, evt):
        config_file = self.tc_ini.GetValue()
        if os.path.exists(config_file):
            self.config_file = config_file

        self.config.read(self.config_file)
        self.init_folders()
    #
    def on_save(self, evt):
        self.temp_dir = self.tc_temp.GetValue()
        self.result_dir = self.tc_mgf.GetValue()
        self.config_file = self.tc_ini.GetValue()

        self.config.set('directories', 'temporal', self.temp_dir)
        self.config.set('directories', 'mgfs', self.result_dir)

        with open(self.config_file, 'w') as f:
            self.config.write(f)
    #
    def on_defaults(self, evt):
        self.set_default_folders()
        self.fill_entries()
    #
    def on_select_temp(self, evt):
        """"""
        dlg = wx.DirDialog(self, "Select Dir", defaultPath='')
    #
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.temp_dir = path
            self.tc_temp.SetValue(path)

    def on_select_mgf(self, evt):
        """"""
        dlg = wx.DirDialog(self, "Select Dir", defaultPath='')
        #
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.result_dir = path
            self.tc_mgf.SetValue(path)

    def on_select_file(self, evt):
        """"""
        wildcard = "Config files (*.ini)|*.ini| all (*.*)|*.*"
        dlg = wx.FileDialog(self, "Select File", defaultDir='',
                            wildcard=wildcard,
                            style=wx.FD_OPEN)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            #
            self.config_file = path
            self.tc_ini.SetValue(path)

#    def on_check(self, evt):
#        print 'temporal ', self.temp_dir
#        print 'mgfs ', self.result_dir
#        print 'ini ', self.config_file
#
#
class AFrame(wx.Frame):
    def __init__(self, *args, **kwargs):
        wx.Frame.__init__(self, *args, **kwargs)
        self.folder = MyFolders(self)


if __name__ == "__main__":

    app = wx.App(0)
    wx.InitAllImageHandlers()
    frame_1 = AFrame(None)
    # noinspection PyUnresolvedReferences
    app.SetTopWindow(frame_1)
    frame_1.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
