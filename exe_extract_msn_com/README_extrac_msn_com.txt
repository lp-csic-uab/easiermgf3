This folder is for the extract_msn_com executable and needed libraries.

The folder must contain:

- extract_msn_com.exe (extract_msn ver. 5.0)
- UnifiedFile.dll (ver. 2008)

Both files can be downloaded in a zip from Thermo webpage at:
- http://sjsupport.thermofinnigan.com/public/detail.asp?id=626
the direct download url is:
- http://sjsupport.thermofinnigan.com/download/files/ExtractMSnCom.zip


To run the program it is also necessary to install following Thermo libraries provided by Xcalibur or MSFileReader:

- Fileio.dll
- Fregistry.dll
- XRawfile2.dll

MSFileReader is a standalone installation of XRawfile2.dll (for Xcalibur), which permits
programmatic access of Thermo data files via a COM interface. 
This program can be downloaded from:
- http://sjsupport.thermofinnigan.com/public/detail.asp?id=703
the direct download url is:
- http://sjsupport.thermofinnigan.com/download/files/MSFileReaderSetup.zip
