---

**WARNING!**: This is the *Old* source-code repository for EasierMGF3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/easiermgf3/) located at https://sourceforge.net/p/lp-csic-uab/easiermgf3/**  

---  
  
  
![https://lh5.googleusercontent.com/-KGa41MkYfRo/TzFczP_DpMI/AAAAAAAAAQU/sjGC4JtmD7U/s800/EasierMgf%2520logo.png](https://lh5.googleusercontent.com/-KGa41MkYfRo/TzFczP_DpMI/AAAAAAAAAQU/sjGC4JtmD7U/s800/EasierMgf%2520logo.png)


---

**WARNING!**: This is the *Old* source-code repository for EasierMGF3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/easiermgf3/) located at https://sourceforge.net/p/lp-csic-uab/easiermgf3/**  

---  
  
  
**Table Of Contents:**

[TOC]

#### Description
**`EasierMgf`** is a rich GUI based, [Python](http://en.wikipedia.org/wiki/Python_%28programming_language%29) application that converts non-standard RAW Thermo Files in [MGF (Mascot Generic Format)](http://www.matrixscience.com/help/data_file_help.html) Files:

![https://lh4.googleusercontent.com/-l091bF7bxEc/TzFapWuJCLI/AAAAAAAAAPo/WEokpIdzXYE/s800/EasierMgf%2520function.png](https://lh4.googleusercontent.com/-l091bF7bxEc/TzFapWuJCLI/AAAAAAAAAPo/WEokpIdzXYE/s800/EasierMgf%2520function.png)

This allows to easily use the data contained in RAW Thermo files with a great variety of proteomic tools (such as OMSSA, Sequest, ...) that can't read RAW files, but accept MGF files.

![https://lh5.googleusercontent.com/-dTwzG1TKqc0/TzFdc4uIDNI/AAAAAAAAAQo/zQ0aiEw3Uxc/s800/EasierMgf%2520screenshot%25201.png](https://lh5.googleusercontent.com/-dTwzG1TKqc0/TzFdc4uIDNI/AAAAAAAAAQo/zQ0aiEw3Uxc/s800/EasierMgf%2520screenshot%25201.png)

#### Installation

Tested in Windows XP 32 bits and Windows 7 64 bits.

##### External Requirements

`EasierMgf` can use four different converters, and each one have their external requirements:

###### - Extract\_msn (v 4.0) :

Compatible with `Thermo Xcalibur 2.0.7` (LTQ XL).

To make this converter work, the folder `exe_extract_msn\` must contain the extract\_msn  executable (ver. 4.0) and needed libraries:

  * `extract_msn.exe` (extract\_msn ver. 4.0) (http://sjsupport.thermofinnigan.com/public/index_download_customer.asp)
  * `Fileio.dll`
  * `Fregistry.dll`
  * `UnifiedFile.dll` (ver. 2007) (http://www.matrixscience.com/help/instruments_xcalibur.html#MSF)

All those files can be obtained from a `Thermo Xcalibur 2.0.7` installation (Folder `C:\Xcalibur\system\programs`),
altought some of them (`Fileio.dll` , `Fregistry.dll`) can also be obtained from [MSFileReader package](http://sjsupport.thermofinnigan.com/download/files/MSFileReaderSetup.zip), which can be downloaded from http://sjsupport.thermofinnigan.com/download/files/MSFileReaderSetup.zip

###### - Extract\_msn\_com (v 5.0) :

To make this converter work, the folder `exe_extract_msn_com\` must contain the extract\_msn\_com executable and needed libraries:

  * `extract_msn_com.exe` (extract\_msn ver. 5.0)
  * `UnifiedFile.dll` (ver. 2008)

Both files can be downloaded as a zip compressed file from [Thermo web page](http://sjsupport.thermofinnigan.com/public/detail.asp?id=626), or using [this direct download link](http://sjsupport.thermofinnigan.com/download/files/ExtractMSnCom.zip).

To run the extract\_msn\_com executable it is also necessary to install following Thermo libraries provided by `Thermo Xcalibur` or `MSFileReader package`:

  * `Fileio.dll`
  * `Fregistry.dll`
  * `XRawfile2.dll`

[MSFileReader](http://sjsupport.thermofinnigan.com/download/files/MSFileReaderSetup.zip) is a standalone installation of XRawfile2.dll (for Xcalibur), which permits
programmatic access of Thermo data files via a COM interface.
`MSFileReader package` can be downloaded from its [web page](http://sjsupport.thermofinnigan.com/public/detail.asp?id=703), or using [this direct download link](http://sjsupport.thermofinnigan.com/download/files/MSFileReaderSetup.zip).


###### - ReAdW :

To make this converter work, the folder `exe_readw\` must contain the `ReAdW executable` (ver. 4.3.1, 2009; available from the [Trans-Proteome Pipeline](http://tools.proteomecenter.org/wiki/index.php?title=Software:TPP)) and needed libraries:

  * `ReAdW.exe`
  * `zlib1.dll`

ReAdW Authors:
_Natalie Tasman_ (SPC/ISB), with _Jimmy Eng_, _Brian Pratt_, and _Matt Chambers_, based on orignal work by _Patrick Pedriolli_.

More information:
http://tools.proteomecenter.org/wiki/index.php?title=Software:ReAdW

Download from:
http://sourceforge.net/projects/sashimi/files/ReAdW%20%28Xcalibur%20converter%29/ReAdW-4.3.1/ReAdW-4.3.1.zip/download

`zlib1.dll` (ver. 1.2.3) can be obtained from http://www.zlib.net/


###### - ReAdW4Mascot2 :

To make this converter work, the folder `exe_readw4mascot2\` must contain the NIST ReAdw4Mascot2 executable (ver. 20091016a; available from the [NIST MSQC Pipeline](http://peptide.nist.gov/software/nist_msqc_pipeline/NIST_MSQC_Pipeline.html)) and needed libraries:

  * `ReAdw4Mascot2.exe` (ver. 20091016a)
  * `zlib1.dll`

`ReAdW4Mascot2` Author:
The program belongs to _Patrick Pedrioli_.
`ReAdW4Mascot2` is part of the **NIST MSQC Pipeline**, which can be downloaded from [its web page](http://peptide.nist.gov/software/nist_msqc_pipeline/NIST_MSQC_Pipeline.html).

`zlib1.dll` (ver. 1.2.3) can be obtained from http://www.zlib.net/

##### From Windows Installer

  1. Download `EsierMGF_x.y_setup.exe` [Windows Installer](https://bitbucket.org/lp-csic-uab/easiermgf/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `EasierMgf` by double-clicking on the `EasierMgf` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install [Python](http://www.python.org/) and other third party software indicated in [Dependencies](#markdown-header-source-dependencies), as required.
  1. Download `EasierMgf` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/easiermgf/src).
  1. Download `commons` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/commons/src).
  1. Copy the `EasierMgf` and `commons` folders in your Python `site-packages` or in another folder in the `python path`. If you only plan to run the program from a particular user, you will need only to put `EasierMgf` folder inside a user folder and then the `commons` folder inside `EasierMgf` folder.
  1. Run `EasierMgf` by double-clicking on the `easier_main.pyw` module, or typing  `python easier_main.pyw`  on the command line.

###### _Source Dependencies:_

  * [Python](http://www.python.org/) 2.7 (not tested with other versions)
  * [wxPython](http://www.wxpython.org/) 2.8.11.0 - 2.8.11.2
  * [commons package](https://bitbucket.org/lp-csic-uab/commons/src) (from LP CSIC/UAB Bitbucket [repository](https://bitbucket.org/lp-csic-uab/commons/src))

Third-party software and package versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

#### Download

You can download the Windows Installer for the last version of **`EasierMgf`** [here](https://bitbucket.org/lp-csic-uab/easiermgf/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/easiermgf/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code


---

**WARNING!**: This is the *Old* source-code repository for EasierMGF3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/easiermgf3/) located at https://sourceforge.net/p/lp-csic-uab/easiermgf3/**  

---  
  
  
