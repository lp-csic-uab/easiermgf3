This folder is for the extract_msn  executable (ver. 4.0) and needed libraries.
The folder must contain:

- extract_msn.exe  (extract_msn ver. 4.0) (http://sjsupport.thermofinnigan.com/public/index_download_customer.asp)
- Fileio.dll
- Fregistry.dll
- UnifiedFile.dll  (ver. 2007) (http://www.matrixscience.com/help/instruments_xcalibur.html#MSF)

All those files can be obtained from a Thermo Xcalibur 2.0.7 installation (Folder C:\Xcalibur\system\programs).
