from literate import Literate
from readw4mascot2 import ReAdw4Mascot2Panel
from extractmsnclassic import ExtractMsnClassicPanel
from extractmsncom import ExtractMsnComPanel
from readw import ReAdWPanel
#
#
class LiterateReAdW(ReAdWPanel, Literate):
    """"""
    hints = {
        'centroid': ('ckbx_centroid',
                     ('Centroid all scans (MS1 and MS2) meaningful only '
                      'if data was acquired in profile mode'),
                     1),
        'precursorFromFilterLine': (
                'ckbx_precursorFromFilterLine',
                ('try to get the precursor MZ value from'
                 ' the Thermo "filter line" text;\n\n'
                 'Only use this if you have a good reason!\n'
                 'Otherwise, the program first will try to obtain a more\n'
                 'accurate mass from the "Monoisotopic M/Z:" "trailer value"')),
        'compress': ('ckbx_compress',
                     ('Use zlib for compressing peaks\n\n'
                      'Do not use when conversion to mgfs is required')),
        'verbose': ('ckbx_verbose',
                    'verbose trace.'),
        'gzip': ('ckbx_gzip',
                 ('gzip the output file (independent of peak compression)\n\n'
                  'Do not use when conversion to mgfs is required')),
        'keep': ('xckbx_keep',
                 'keep original ReAdW mzXML file'),
        'mzml': ('xckbx_mzml',
                 'Produce mzML output.'
                 ' If checked no mzXML nor mgfs are produced')}

    def __init__(self, *args, **kwargs):
        """"""
        ReAdWPanel.__init__(self, *args, **kwargs)
        Literate.__init__(self, LiterateReAdW.hints)
#
#
#
class LiterateReAdw4Mascot2(ReAdw4Mascot2Panel, Literate):
    """"""
    hints = {'c': ('ckbx_centroid',
                   'Centroid the data (meaningful only if RAW data is profile)',
                   1),
             'XmlOrbiMs1Profile': ('ckbx_orbims1prof',
                                   'Save Profile FTMS ms1 spectra'
                                   ' (override -c) into mzXml'),
             'Compression': ('ckbx_compression',
                             'Use zlib for compressing peaks in mzXML.'),
             'Rulers': ('ckbx_rulers',
                        'Attempt compression using m/z ruler.'),
             'ms1': ('ckbx_outputms1',
                     'Also output MS1 spectra and precursor scan no. for MS2.'),
             'sep1': ('ckbx_separatedms1',
                      'Output MS1 spectra in a separate outfile.RAW.MS1',
                      '1'),
             'metadata': ('ckbx_metadata',
                          'Output Instrument Method, Tune Data,'
                          ' and scan Trailer info.'),
             'NoMgf': ('ckbx_nomgf',
                       'Do not produce MGF output.'),
             'NoMzXML': ('ckbx_nomzxml',
                         'Do not produce MzXml output.',
                         '0'),
             'ChargeMgfOrbi': ('ckbx_chargemgforbi',
                               'Include CHARGE in MGF output for'
                               ' LTQ Orbitrap.'),
             'MonoisoMgfOrbi': ('ckbx_monoisomgforbi',
                                'In MGF, PEPMASS=Monoisotopic m/z for'
                                ' LTQ Orbitrap instruments'),
             'MonoisoMgf': ('ckbx_monoisomgf',
                            'In MGF, PEPMASS=Monoisotopic m/z if available'),
             'FixPepmass': ('ckbx_fixpepmass',
                            ('Replace PEPMASS and CHARGE with better ones if'
                             ' found (Orbitrap and FT only)')),
             'MaxPI': ('ckbx_maxpi',
                       'Include max precursor intensity'),
             'PIvsRT_Debug': ('ckbx_pivsrt_debug',
                              'Include precursor intensity vs RT lines,'
                              ' verbose'),
             'PIvsRT': ('ckbx_pivsrt',
                        'Include precursor intensity vs RT lines'),
             'SampleInfo': ('ckbx_sampleinfo',
                            'Output Sample Info into MGF'),
             'NoPeaks': ('ckbx_nopeaks',
                         'Do not output mass spectral peaks'),
             'NoPeaks1': ('ckbx_nopeaks1',
                          'Output ms1 spectra without mass spectral peaks'),
             'xpw': ('tc_xpw_width',
                     ('xpm width\n'
                      'width = Min precursor XIC Peak Width (seconds);'
                      ' default=0.0\n'
                      '(max. moving average window for preliminary ms1 XIC peak'
                      ' detection) = 0.5 * (width)')),
             'xpm': ('tc_xpm_points',
                     ('xpm points\n'
                      'points = min. number of ms1 XIC points per minute\n'
                      '  >19 for condensing the data\n'
                      '  e.g. 32 means 104 points per 3.25 min.\n'
                      '  default=no condensing')),
             'AutoOrbi': ('ckbx_autoorbi',
                          'automatically pick the right options based on'
                          ' instrument.'),
             'keep_format': ('xckbx_keep',
                             'keep original ReAdw4Mascot2 mgf format')}

    def __init__(self, *args, **kwargs):
        """"""
        ReAdw4Mascot2Panel.__init__(self, *args, **kwargs)
        Literate.__init__(self, LiterateReAdw4Mascot2.hints)
#
#
#
class LiterateExtractMsnClassic(ExtractMsnClassicPanel, Literate):
    """"""
    hints = {
        'F': ('tc_first_scan',
              'INT specifying the first scan'),
        'L': ('tc_last_scan',
              'INT specifying the last scan'),
        'B': ('tc_min_mass',
              ('Bottom MW for datafile creation\n'
               'The value must be between 0 and 1000.')),
        'T': ('tc_max_mass',
              ('Top MW for datafile creation\n'
               'The value must be between 100 and 10000.')),
        'M': ('tc_prec_tol',
              ('Precursor mass tolerance for grouping (default=1.4)\n\n'
               'The precursor ion mass tolerance for DTA generation.\n'
               'If two precursor ion masses differ by less than the\n'
               'tolerance value, they are considered to be the same\n'
               'precursor ion for purposes of DTA generation.\n'
               'The value must be between 0 and 4.'),
              '1.4'),
        'S': ('tc_group_scan',
              ('Number of allowed different intermediate scans for grouping'
               ' (default=1)\n\n'
               'Sets a limit for the maximum number of intervening MS/MS\n'
               ' scans allowed between spectra with the same precursor\n'
               ' ion mass.\n'
               'The precursor ion mass tolerance value is specified in the\n'
               'Precursor Ion Tolerance text box which also still allows\n'
               ' those spectra to be grouped into a single SEQUEST .dta file.\n'
               'The grouping behavior is determined from this parameter as'
               ' follows:\n'
               '    0    -> No grouping of scans.\n'
               '    1    -> Only adjacent scans can be grouped.\n'
               '    >=2 -> A maximum of n-1 intervening scans are\n'
               '             allowed between spectra to be grouped'),
              '1'),
        'C': ('tc_charge',
              'INT specifying the charge state to use'),
        'G': ('tc_min_group_count',
              ('Minimum number of related grouped scans needed for a .dta file'
               ' (default=2)\n\n'
               'The minimum number of scans with the same precursor ion mass\n'
               '(to within the precursor mass tolerance value displayed in\n'
               ' the Precursor Mass text box) and charge state required to\n'
               ' create a SEQUEST DTA file.\n'
               'The value must be between 0 and 1000'),
              '1'),
        'I': ('tc_min_ion_count',
              (
              'Minimum number of ions needed for a .dta file (default=0)\n\n'
              'The minimum number of ions required before the spectrum is'
              ' included in the search.\n'
              'The value must be between 0 and 1000.'),
              '0'),
        'E': ('tc_int_thresh',
              ('Intensity threshold (float)\n\n'
               'The minimum ion current threshold used to DTA generation.\n'
               'Specifies the minimum total ion current necessary for the\n'
               'spectra information to be included in a DTA file'),
              '2'),
        'R': ('tc_min_sn_ratio',
              (
              'FLOAT specifying the minimum signal-to-noise value\n'
              'needed for a peak to be written to a .dta file (default=3)'),
              '0.5'),
        'r': ('tc_min_peaks',
              ('INT specifying the minimum number of major peaks\n'
               '(peaks above S/N threshold) needed for a .dta file'
               ' (default=5)'),
              '5'),
        'A': ('tc_options',
              ('A string containing any of the options\n'
               '  T: use template\t\t'     'F: use discrete Fourier transform\n'
               '  E: use Engs algorithm\t\t'    'H: use scan header\n'
               '  M: use MSMS count\n'
               '  O: override header charge state\n'
               '  S: create summary file\t\t'   'L: create log file\n'
               '  D: create both files\t\t'     'C: create MSMS count file\n'
               '  A: find CS even for nonzero headers\n\n'
               '  tfehm: include algorithm output in summary file even'
               ' if not called\n\n'
               '  [NOTE: This version of the program has a default string'
               ' of -AHTFEMAOSC,\n'
               '  but if -A option is used all desired parameters must be'
               ' specified]'),
              'HTFEMAOSC'),
        'keep_dtas': ('xckbx_keep',
                      'keep dtas')}

    def __init__(self, *args, **kwargs):
        ExtractMsnClassicPanel.__init__(self, *args, **kwargs)
        Literate.__init__(self, self.hints)
#
#
#
class LiterateExtractMsnCom(ExtractMsnComPanel, Literate):
    """"""
    hints = {
        'F': ('tc_first_scan',
              'INT specifying the first scan'),
        'L': ('tc_last_scan',
              'INT specifying the last scan'),
        'B': ('tc_min_mass',
              ('Bottom MW for datafile creation\n'
               'The value must be between 0 and 1000.')),
        'T': ('tc_max_mass',
              ('Top MW for datafile creation\n'
               'The value must be between 100 and 10000.')),
        'M': ('tc_prec_tol',
              ('Precursor mass tolerance for grouping (default=1.4)\n\n'
               'The precursor ion mass tolerance for DTA generation.\n'
               'If two precursor ion masses differ by less than the\n'
               'tolerance value, they are considered to be the same\n'
               'precursor ion for purposes of DTA generation.\n'
               'The value must be between 0 and 4.'),
              '1.4'),
        'S': ('tc_group_scan',
              ('Number of allowed different intermediate scans for grouping'
               ' (default=1)\n\n'
               'Sets a limit for the maximum number of intervening MS/MS\n'
               ' scans allowed between spectra with the same precursor\n'
               ' ion mass.\n'
               'The precursor ion mass tolerance value is specified in the\n'
               'Precursor Ion Tolerance text box which also still allows \n'
               'those spectra to be grouped into a single SEQUEST .dta file.\n'
               'The grouping behavior is determined from this parameter as'
               ' follows:\n'
               '    0    -> No grouping of scans.\n'
               '    1    -> Only adjacent scans can be grouped.\n'
               '    >=2 -> A maximum of n-1 intervening scans are\n'
               '             allowed between spectra to be grouped'),
              '1'),
        'C': ('tc_charge',
              'INT specifying the charge state to use'),
        'G': ('tc_min_group_count',
              ('Minimum number of related grouped scans needed for a .dta file'
               ' (default=1)\n\n'
               'The minimum number of scans with the same precursor ion mass\n'
               '(to within the precursor mass tolerance value displayed in\n'
               ' the Precursor Mass text box) and charge state required\n'
               ' to create a SEQUEST DTA file.\n'
               'The value must be between 0 and 1000'),
              '1'),
        'I': ('tc_min_ion_count',
              ('Minimum number of ions needed for a .dta file (default=0)\n\n'
               'The minimum number of ions required before the spectrum is\n'
               ' included in the search.\n'
               'The value must be between 0 and 1000.'),
               '0'),
        'E': ('tc_int_thresh',
              ('Intensity threshold (float)\n\n'
               'The minimum ion current threshold used to DTA generation.\n'
               'Specifies the minimum total ion current necessary for the\n'
               'spectra information to be included in a DTA file'),
              '2'),
        'R': ('tc_min_sn_ratio',
              ('FLOAT specifying the minimum signal-to-noise value\n'
               'needed for a peak to be written to a .dta file (default=3)'),
              '0.5'),
        'r': ('tc_min_peaks',
              ('INT specifying the minimum number of major peaks\n'
               '(peaks above S/N threshold) needed for a .dta file'
               ' (default=5)'),
              '5'),
        'keep_dtas': ('xckbx_keep',
                      'keep dtas')}

    def __init__(self, *args, **kwargs):
        ExtractMsnComPanel.__init__(self, *args, **kwargs)
        Literate.__init__(self, self.hints)
