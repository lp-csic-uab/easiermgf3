#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
easier_raw_class (psearch_15)
15 december 2010
#
Class that extracts ms data, tr, scan, ms level, etc... from each
Thermo .raw file.
#
"""
#
import win32com.client as wc
import pywintypes
import time
#
#
#
#noinspection PyUnresolvedReferences
class RawFileExtractor():
    """Extractor with XRawfile COM object.

    XRawfile.XRawfile access raw file data without loading it in full. However
    it doesnt allow to get the spectrum.

    """
    def __init__(self, rawfile):
        """Constructor. Opens the Xraw object

        Uses a while loop to retry after a failure (p.e. produced by a big file)

        """
        #
        n = 0
        while n < 3:
            xraw = None
            try:
                xraw = wc.Dispatch('XRawFile.XrawFile.1')
                xraw.Open(rawfile)
                self.success = True
                break
            except pywintypes.com_error:
                print('error XRawFile')
                del xraw
                time.sleep(5)
                n += 1
        #if it doesn't go out through break
        else:
            self.success = False
            return
        #
        self.xraw = xraw
        self.xraw.SetCurrentController(0, 1)
        print('Initiated XRawFile')
    #
    def get_rt(self, item):
        """(scan, rt) <--> (0, 0.0046666666666666671)"""
        return self.xraw.RTFromScanNum(item, None)
    #
    def get_filter(self, item):
        """+ c Full ms2 363.30@cid40.00 [150.00-375.00]"""
        try:
            return self.xraw.GetFilterForScanNum(item, None)
        except (AttributeError, pywintypes.com_error):
            return "err1"
    #
    def get_mode_and_ms_level(self, item):
        """Extracts mslevel from the spectrum filter

        FTMS + c NSI d Full ms2 1070.52@hcd59.00 [110.00-2000.00]

        """
        text = self.get_filter(item)
        index = text.find('ms')
        if index > -1:
            level = text[index:index + 3]
        else:
            level = "err2"

        index = text.find('@')

        if index > -1:
            mode = text[index + 1:index + 4]
        else:
            mode = "err2"

        return mode, level
    #
    def close(self):
        """"""
        if self.xraw:
            self.xraw.Close()

#
#
#noinspection PyUnusedLocal
class MockExtractor:
    """"""
    def __init__(self, file=None):
        """"""
        self.success = 1
    #
    def get_rt(self, item):
        """"""
        return 0
    #
    def get_filter(self, item):
        """"""
        return "mock"
    #
    def get_mode_and_ms_level(self, item):
        """"""
        return "mock", '0'
    #
    def close(self):
        """"""
        pass
#


if __name__ == '__main__':
    import os
    test_dir = "C:/python26/programas/_easier/test_mgf"
    #
    test_file = os.path.join(test_dir, "CLS_2010_156_01_Velos.raw")  # doesnt work (Velos)
    # test_file = os.path.join(test_dir, "test_Fosfomix.RAW")         # OK
    # test_file = os.path.join(test_dir, "test_mgf/test_DIGE.RAW")    # OK
    test_file = os.path.join(test_dir, "test_mgf/Orbi_HCD_CID_NL_4.raw")  # OK (scans 0-50)

    raw_ex = RawFileExtractor(test_file)
    # print 'raw', raw_ex
    if raw_ex.success:
        for item in range(0, 50):
        # for item in range(1,15):
            print("spectrum", item)
            print(raw_ex.get_filter(item))
            print("ms_level: ", raw_ex.get_mode_and_ms_level(item))
            print("Rt -->", raw_ex.get_rt(item))
    else:
        print('failure')

    input()

#    test_Fosfomix
#
#    initiated
# ......................................
#    spectrum 166
#    FTMS + c NSI Full ms [400.00-1800.00]
#    ms_level:  ms
#    Rt --> 13.0479766667
#    spectrum 167
#    FTMS + c NSI Full ms [400.00-1800.00]
#    ms_level:  ms
#    Rt --> 13.06552
#    spectrum 168
#    FTMS + c NSI Full ms [400.00-1800.00]
#    ms_level:  ms
#    Rt --> 13.0833166667
#    spectrum 169
#    ITMS + c NSI d Full ms2 835.34@cid30.00 [215.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.094445
#    spectrum 170
#    ITMS + c NSI d Full ms3 835.34@cid30.00 786.56@cid35.00 [205.00-1585.00]
#    ms_level:  ms3
#    Rt --> 13.1235816667
#    spectrum 171
#    ITMS + c NSI d Full ms2 835.83@cid30.00 [220.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.1556533333
#    spectrum 172
#    ITMS + c NSI d Full ms3 835.83@cid30.00 786.61@cid35.00 [205.00-1585.00]
#    ms_level:  ms3
#    Rt --> 13.184675
#    spectrum 173
#    FTMS + c NSI Full ms [400.00-1800.00]
#    ms_level:  ms
#    Rt --> 13.2173116667
#    spectrum 174
#    ITMS + c NSI d Full ms2 835.34@cid30.00 [215.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.2241333333
#    spectrum 175
#    ITMS + c NSI d Full ms3 835.34@cid30.00 786.62@cid35.00 [205.00-1585.00]
#    ms_level:  ms3
#    Rt --> 13.25311
#    spectrum 176
#    ITMS + c NSI d Full ms2 835.84@cid30.00 [220.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.2851733333
#    spectrum 177
#    ITMS + c NSI d Full ms3 835.84@cid30.00 786.61@cid35.00 [205.00-1585.00]
#    ms_level:  ms3
#    Rt --> 13.3142733333
#    spectrum 178
#    ITMS + c NSI d Full ms2 836.34@cid30.00 [220.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.3463416667
#    spectrum 179
#    ITMS + c NSI d Full ms3 836.34@cid30.00 787.13@cid35.00 [205.00-1585.00]
#    ms_level:  ms3
#    Rt --> 13.3753
#    spectrum 180
#    ITMS + c NSI d Full ms2 557.23@cid30.00 [140.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.407545
#    spectrum 181
#    ITMS + c NSI d Full ms3 557.23@cid30.00 524.82@cid35.00 [130.00-1060.00]
#    ms_level:  ms3
#    Rt --> 13.43701
#    spectrum 182
#    ITMS + c NSI d Full ms2 574.87@cid30.00 [145.00-1735.00]
#    ms_level:  ms2
#    Rt --> 13.4672116667
#    spectrum 183
#    ITMS + c NSI d Full ms3 574.87@cid30.00 542.37@cid35.00 [135.00-1095.00]
#    ms_level:  ms3
#    Rt --> 13.496945
#    spectrum 184
#    ITMS + c NSI d Full ms2 557.56@cid30.00 [140.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.52738
#    spectrum 185
#    ITMS + c NSI d Full ms3 557.56@cid30.00 524.88@cid35.00 [130.00-1060.00]
#    ms_level:  ms3
#    Rt --> 13.5568416667
#    spectrum 186
#    ITMS + c NSI d Full ms2 575.20@cid30.00 [145.00-1740.00]
#    ms_level:  ms2
#    Rt --> 13.587055
#    spectrum 187
#    FTMS + c NSI Full ms [400.00-1800.00]
#    ms_level:  ms
#    Rt --> 13.6165666667
#    spectrum 188
#    ITMS + c NSI d Full ms2 836.34@cid30.00 [220.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.6232116667
#    spectrum 189
#    ITMS + c NSI d Full ms3 836.34@cid30.00 787.15@cid35.00 [205.00-1585.00]
#    ms_level:  ms3
#    Rt --> 13.6521783333
#    spectrum 190
#    ITMS + c NSI d Full ms2 557.23@cid30.00 [140.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.684475
#    spectrum 191
#    ITMS + c NSI d Full ms3 557.23@cid30.00 524.81@cid35.00 [130.00-1060.00]
#    ms_level:  ms3
#    Rt --> 13.7139516667
#    spectrum 192
#    ITMS + c NSI d Full ms2 557.56@cid30.00 [140.00-1685.00]
#    ms_level:  ms2
#    Rt --> 13.74421
#    spectrum 193
#    ITMS + c NSI d Full ms3 557.56@cid30.00 524.88@cid35.00 [130.00-1060.00]
#    ms_level:  ms3
#    Rt --> 13.7737133333
#    spectrum 194
#    FTMS + c NSI Full ms [400.00-1800.00]
#    ms_level:  ms
#    Rt --> 13.8044083333
#    spectrum 195
#    ITMS + c NSI d Full ms2 888.25@cid30.00 [230.00-1790.00]
#    ms_level:  ms2
#    Rt --> 13.8109033333
#    spectrum 196
#    ITMS + c NSI d Full ms2 888.75@cid30.00 [230.00-1790.00]
#    ms_level:  ms2
#    Rt --> 13.8400616667
#    spectrum 197
#    FTMS + c NSI Full ms [400.00-1800.00]
#    ms_level:  ms
#    Rt --> 13.87009
#    spectrum 198
#    ITMS + c NSI d Full ms2 574.87@cid30.00 [145.00-1735.00]
#    ms_level:  ms2
#    Rt --> 13.876695
#    spectrum 199
#    ITMS + c NSI d Full ms3 574.87@cid30.00 542.37@cid35.00 [135.00-1095.00]
#    ms_level:  ms3
#    Rt --> 13.9063933333


#    Orbi_HCD_CID_NL_4.raw
#
#    initiated
#    spectrum 0
#
#    ms_level:  err2
#    Rt --> 0.0
#    spectrum 1
#    FTMS + c NSI Full ms [500.00-2000.00]
#    ms_level:  ms
#    Rt --> 0.00533166666667
#    spectrum 2
#    FTMS + c NSI d Full ms2 536.17@hcd59.00 [100.00-550.00]
#    ms_level:  ms2
#    Rt --> 0.0210516666667
#    spectrum 3
#    ITMS + c NSI d Full ms2 536.17@cid30.00 [135.00-550.00]
#    ms_level:  ms2
#    Rt --> 0.073515
#    spectrum 4
#    ITMS + c NSI d Full ms3 536.17@cid30.00 502.90@cid30.00 [125.00-1520.00]
#    ms_level:  ms3
#    Rt --> 0.0983083333333
#    spectrum 5
#    ITMS + c NSI d Full ms2 519.14@cid30.00 [130.00-530.00]
#    ms_level:  ms2
#    Rt --> 0.150436666667
#    spectrum 6
#    ITMS + c NSI d Full ms2 610.18@cid30.00 [155.00-625.00]
#    ms_level:  ms2
#    Rt --> 0.192918333333
#    spectrum 7
#    ITMS + c NSI d Full ms2 593.16@cid30.00 [150.00-605.00]
#    ms_level:  ms2
#    Rt --> 0.235606666667
#    spectrum 8
#    FTMS + c NSI Full ms [500.00-2000.00]
#    ms_level:  ms
#    Rt --> 0.263483333333
#    spectrum 9
#    FTMS + c NSI d Full ms2 550.22@hcd59.00 [100.00-565.00]
#    ms_level:  ms2
#    Rt --> 0.279551666667
#    spectrum 10
#    ITMS + c NSI d Full ms2 550.22@cid30.00 [140.00-565.00]
#    ms_level:  ms2
#    Rt --> 0.33101
#    spectrum 11
#    ITMS + c NSI d Full ms2 684.20@cid30.00 [175.00-695.00]
#    ms_level:  ms2
#    Rt --> 0.375773333333
#    spectrum 12
#    ITMS + c NSI d Full ms2 624.24@cid30.00 [160.00-635.00]
#    ms_level:  ms2
#    Rt --> 0.418606666667
#    spectrum 13
#    ITMS + c NSI d Full ms2 539.16@cid30.00 [135.00-550.00]
#    ms_level:  ms2
#    Rt --> 0.461303333333
#    spectrum 14
#    FTMS + c NSI Full ms [500.00-2000.00]
#    ms_level:  ms
#    Rt --> 0.488983333333
#    spectrum 15
#    FTMS + c NSI d Full ms2 533.19@hcd59.00 [100.00-545.00]
#    ms_level:  ms2
#    Rt --> 0.504055
#    spectrum 16
#    ITMS + c NSI d Full ms2 533.19@cid30.00 [135.00-545.00]
#    ms_level:  ms2
#    Rt --> 0.556346666667
#    spectrum 17
#    ITMS + c NSI d Full ms2 667.18@cid30.00 [170.00-680.00]
#    ms_level:  ms2
#    Rt --> 0.601108333333
#    spectrum 18
#    ITMS + c NSI d Full ms2 607.21@cid30.00 [155.00-620.00]
#    ms_level:  ms2
#    Rt --> 0.64394
#    spectrum 19
#    ITMS + c NSI d Full ms2 522.14@cid30.00 [130.00-535.00]
#    ms_level:  ms2
#    Rt --> 0.686805
#    spectrum 20
#    FTMS + c NSI Full ms [500.00-2000.00]
#    ms_level:  ms
#    Rt --> 0.71449
#    spectrum 21
#    FTMS + c NSI d Full ms2 669.18@hcd59.00 [100.00-680.00]
#    ms_level:  ms2
#    Rt --> 0.730718333333
#    spectrum 22
#    ITMS + c NSI d Full ms2 669.18@cid30.00 [170.00-680.00]
#    ms_level:  ms2
#    Rt --> 0.781965
#    spectrum 23
#    ITMS + c NSI d Full ms2 503.11@cid30.00 [125.00-515.00]
#    ms_level:  ms2
#    Rt --> 0.827131666667
#    spectrum 24
#    ITMS + c NSI d Full ms2 613.18@cid30.00 [155.00-625.00]
#    ms_level:  ms2
#    Rt --> 0.86978
#    spectrum 25
#    ITMS + c NSI d Full ms2 741.19@cid30.00 [190.00-755.00]
#    ms_level:  ms2
#    Rt --> 0.912433333333
#    spectrum 26
#    FTMS + c NSI Full ms [500.00-2000.00]
#    ms_level:  ms
#    Rt --> 0.940661666667
#    spectrum 27
#    FTMS + c NSI d Full ms2 596.16@hcd59.00 [100.00-610.00]
#    ms_level:  ms2
#    Rt --> 0.957051666667
#    spectrum 28
#    ITMS + c NSI d Full ms2 596.16@cid30.00 [150.00-610.00]
#    ms_level:  ms2
#    Rt --> 1.00820666667
#    spectrum 29
#    ITMS + c NSI d Full ms2 758.22@cid30.00 [195.00-770.00]
#    ms_level:  ms2
#    Rt --> 1.05310333333
#    spectrum 30
#    ITMS + c NSI d Full ms2 687.20@cid30.00 [175.00-700.00]
#    ms_level:  ms2
#    Rt --> 1.09609666667
#    spectrum 31
#    FTMS + c NSI d Full ms2 1070.52@hcd59.00 [110.00-2000.00]
#    ms_level:  ms2
#    Rt --> 1.12177333333
#    spectrum 32
#    ITMS + c NSI d Full ms2 1070.52@cid30.00 [280.00-2000.00]
#    ms_level:  ms2
#    Rt --> 1.168425
#    spectrum 33
#    FTMS + c NSI Full ms [500.00-2000.00]
#    ms_level:  ms
#    Rt --> 1.20215
#    spectrum 34
#    FTMS + c NSI d Full ms2 681.23@hcd59.00 [100.00-695.00]
#    ms_level:  ms2
#    Rt --> 1.21872166667
#    spectrum 35
#    ITMS + c NSI d Full ms2 681.23@cid30.00 [175.00-695.00]
#    ms_level:  ms2
#    Rt --> 1.269695
#    spectrum 36
#    ITMS + c NSI d Full ms2 698.26@cid30.00 [180.00-710.00]
#    ms_level:  ms2
#    Rt --> 1.31476166667
#    spectrum 37
#    ITMS + c NSI d Full ms2 743.20@cid30.00 [190.00-755.00]
#    ms_level:  ms2
#    Rt --> 1.357595
#    spectrum 38
#    ITMS + c NSI d Full ms2 505.11@cid30.00 [125.00-520.00]
#    ms_level:  ms2
#    Rt --> 1.40080333333
#    spectrum 39
#    FTMS + c NSI Full ms [500.00-2000.00]
#    ms_level:  ms
#    Rt --> 1.42949166667
#    spectrum 40
#    FTMS + c NSI d Full ms2 535.19@hcd59.00 [100.00-550.00]
#    ms_level:  ms2
#    Rt --> 1.44622333333
#    spectrum 41
#    ITMS + c NSI d Full ms2 535.19@cid30.00 [135.00-550.00]
#    ms_level:  ms2
#    Rt --> 1.49723
#    spectrum 42
#    ITMS + c NSI d Full ms3 535.19@cid30.00 502.87@cid30.00 [125.00-1520.00]
#    ms_level:  ms3
#    Rt --> 1.52222166667
